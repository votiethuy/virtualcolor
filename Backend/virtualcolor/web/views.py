#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render,render_to_response,get_object_or_404,redirect
from django.http import HttpResponse,Http404
import json
from ipware.ip import get_ip
from django.template import RequestContext
from django.core.cache import cache
from pusher import Pusher
from django.conf import settings
# Create your views here.
def index(request):
	ip = get_ip(request)
	color = cache.get(ip)
	if color is None:
		color = "ffffff"

	data = {'ip':ip,'color':color,'key':settings.KEY}
	return render_to_response('index.html',data,context_instance=RequestContext(request))

def update_color(request):
	ip = get_ip(request)
	color = request.GET.get('color','#fff')
	cache.set(ip, color, timeout=None)
	color = cache.get(ip)
	pusher = Pusher(app_id=settings.APP_ID, key=settings.KEY, secret=settings.SECRET,timeout=20)
	pusher.trigger(ip, 'changecolor', {u'color': color})
	data ={'e':0,'color':color,'ip':ip}
	json_response = json.dumps(data)
	return HttpResponse(json_response,mimetype='application/json')

def get_color(request):
	ip = get_ip(request)
	color = cache.get(ip)
	data = {'e':0,'color':color,'ip':ip}
	json_response = json.dumps(data)
	return HttpResponse(json_response,mimetype='application/json')

