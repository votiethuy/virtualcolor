from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'virtualcolor.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'web.views.index', name='home'),
    url(r'^updatecolor.html$', view='web.views.update_color', name='update_color'),
    url(r'^getcolor.html$', view='web.views.get_color', name='get_color'),
    url(r'^admin/', include(admin.site.urls)),
)
