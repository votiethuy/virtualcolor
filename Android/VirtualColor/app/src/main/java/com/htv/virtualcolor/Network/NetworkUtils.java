package com.htv.virtualcolor.Network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.apache.http.NameValuePair;
import org.apache.http.conn.util.InetAddressUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by huyvt on 7/1/2014.
 */
public class NetworkUtils {
    private final static int TIMEOUT = 60000; // 60s

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED
                            || info[i].getState() == NetworkInfo.State.CONNECTING) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String getDataFromSv(String url) {
        return getDataFromSvNew(url, TIMEOUT, null);
    }

    public static String getDataFromSv(String url, int timeout) {
        return getDataFromSvNew(url, timeout, null);
    }

    public static String getDataFromSv(String url, List<NameValuePair> data) {
        return getDataFromSvNew(url, TIMEOUT, data);
    }

    public static String getDataFromSvNew(String urlTarget, int timeout,
                                          List<NameValuePair> data) {
        URL url;
        HttpURLConnection connection = null;
        String urlParameters = "&";
        if (data != null) {
            for (NameValuePair item : data) {
                try {
                    if (item != null) {
                        urlParameters += item.getName() + "="
                                + URLEncoder.encode(item.getValue(), "UTF-8")
                                + "&";
                    }
                } catch (UnsupportedEncodingException e) {

                } catch (NullPointerException e) {

                }
            }
            if (urlParameters.startsWith("&")) {
                urlParameters = urlParameters.substring(1);
            }

            if (urlParameters.endsWith("&")) {
                urlParameters.substring(0, urlParameters.length() - 1);
            }
        } else {
            urlParameters = "";
        }

        try {
            // Create connection
            url = new URL(urlTarget);
            connection = (HttpURLConnection) url.openConnection();
            if (urlParameters.length() > 0) {
                connection.setRequestMethod("POST");
            } else {
                connection.setRequestMethod("GET");
            }
            if (urlParameters.length() > 0) {
                connection.setFixedLengthStreamingMode(urlParameters.length());
            } else {
                connection.setChunkedStreamingMode(0);
            }
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    "" + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setReadTimeout(TIMEOUT);
            connection.setConnectTimeout(TIMEOUT);
            connection.setRequestProperty("User-Agent", "virtualcolor");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Connection", "close");
            System.setProperty("http.keepAlive", "false");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            if (urlParameters.length() > 0) {
                connection.setDoOutput(true);
                // Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();
            }
            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (SocketTimeoutException e) {

        } catch (OutOfMemoryError e) {

        } catch (Exception e) {

        } finally {

            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                }
            }
        }
        return null;
    }
}
