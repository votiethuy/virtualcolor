package com.htv.virtualcolor;

import android.content.Context;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.flask.colorpicker.ColorPickerView;

import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.htv.virtualcolor.Network.NetworkUtils;
import com.htv.virtualcolor.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainActivity extends ActionBarActivity {
    private final static String TAG = "MainActivity";
    private View root;
    private int currentBackgroundColor = 0xffffffff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        root = findViewById(R.id.root);
        findViewById(R.id.btn_choose_color).setOnClickListener(v -> {
            final Context context = MainActivity.this;

            ColorPickerDialogBuilder
                    .with(context)
                    .noSliders()
                    .setTitle(getString(R.string.choose_color))
                    .initialColor(currentBackgroundColor)
                    .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                    .density(12)
                    .setOnColorSelectedListener(selectedColor -> {

                    })
                    .setPositiveButton(getString(R.string.ok), (dialog, selectedColor, allColors) -> {
                        changeBackgroundColor(selectedColor);
                    })
                    .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
                    })
                    .build()
                    .show();
        });
    }

    private void initDialog(String color) {
        loadingResultDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        loadingResultDialog.getProgressHelper().setBarColor(Color.parseColor("#" + color));
        loadingResultDialog.setTitleText(getString(R.string.loading));
        loadingResultDialog.setCancelable(false);
        loadingResultDialog.show();
    }

    private void changeBackgroundColor(int selectedColor) {
        String color = Integer.toHexString(selectedColor).toUpperCase();
        Log.v("ChooseColor", "Color " + color);
        currentBackgroundColor = selectedColor;
        root.setBackgroundColor(selectedColor);
        UpdateColorTask updateColorTask = new UpdateColorTask(color);
        updateColorTask.execute();
    }

    SweetAlertDialog loadingResultDialog;

    public class UpdateColorTask extends AsyncTask<Void, Void, Boolean> {

        private final String color;


        UpdateColorTask(String color) {
            this.color = color;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initDialog(color);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            String url = String.format(Constant.URL_UPDATE_COLOR, color.substring(2));
            String response = NetworkUtils.getDataFromSv(url);
            Log.v(TAG, url + "---------------" + response);
            if (response != null) {
                try {
                    JSONObject resp = new JSONObject(response);
                    int e = resp.optInt("e", 1);
                    return e == 0;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);
            if (success) {
                loadingResultDialog
                        .setTitleText(getString(R.string.success))
                        .setContentText(getString(R.string.success_phrase))
                        .setConfirmText(getString(R.string.ok))
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            } else {
                loadingResultDialog
                        .setTitleText(getString(R.string.fail))
                        .setContentText(getString(R.string.fail_phrase))
                        .setConfirmText(getString(R.string.ok))
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
            }
        }

    }
}
